-- Potato
farming.register_plant("farming_plus:potato", {
	description = "Potato Seeds",
	inventory_image = "farming_plus_potato_seed.png",
	steps = 3,
	minlight = 13,
	maxlight = default.LIGHT_MAX,
	fertility = {"grassland"}
})

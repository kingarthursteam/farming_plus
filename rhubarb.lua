-- Rhubarb
farming.register_plant("farming_plus:rhubarb", {
	description = "Rhubarb Seeds",
	inventory_image = "farming_plus_rhubarb_seed.png",
	steps = 3,
	minlight = 13,
	maxlight = default.LIGHT_MAX,
	fertility = {"grassland"}
})

-- main `S` code in init.lua
farming.register_plant("farming_plus:tomato", {
	description = "Tomato Seeds",
	inventory_image = "farming_plus_tomato_seed.png",
	steps = 4,
	minlight = 13,
	maxlight = default.LIGHT_MAX,
	fertility = {"grassland"}
})

-- Orange

farming.register_plant("farming_plus:orange", {
	description = "Orange Seeds",
	inventory_image = "farming_plus_orange_seed.png",
	steps = 4,
	minlight = 13,
	maxlight = default.LIGHT_MAX,
	fertility = {"grassland"}
})

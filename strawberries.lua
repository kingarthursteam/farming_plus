-- Strawberry
farming.register_plant("farming_plus:strawberry", {
	description = "Strawberry Seeds",
	inventory_image = "farming_plus_strawberry_seed.png",
	steps = 4,
	minlight = 13,
	maxlight = default.LIGHT_MAX,
	fertility = {"grassland"}
})

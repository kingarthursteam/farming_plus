-- Carrots
farming.register_plant("farming_plus:carrot", {
	description = "Carrot Seeds",
	inventory_image = "farming_plus_carrot_seed.png",
	steps = 4,
	minlight = 13,
	maxlight = default.LIGHT_MAX,
	fertility = {"grassland"}
})
